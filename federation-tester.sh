#!/bin/bash
#
# This script will do several tests on a given matrix server.
#
# (c) 2019 Kai Kalua <kai@i2pmail. org>
#
# License: GPL v2 (https://www.gnu.org/licenses/old-licenses/gpl-2.0.de.html)
#
# You're allowed to publish the code if you follow every other rule
# of the GPL v2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Changelog
# ----------
# v1.3           - If the given domain to check is an IP literal (not
#                  supported currently), exit the script to avoid misunderstanding
#                - Give only a warning message instead of a failure message,
#                  if there's no .well-known configuraion and no DNS SRV record
#                - Switch from warning to info message, if using the default port
#                  as fallback
#                - If the server response contain multiple Content-Type lines when
#                  checking .well-known/matrix/server, take only the first line
#                - TODO: Redo the .well-known and DNS SRV record handling
#                        because it's NOT totally compliant with the Federation
#                        specification (many thanks to @tulir:maunium.net for
#                        his explanation)
#
# v1.2          - Exit program, if the user want to check an invalid domain
#                 (without a dot)
#               - Check, if the certificate is valid for the domain name, if
#                 there's only a DNS SRV record. Otherwise Synapse will not
#                 federate (even if the certificate is valid for the hostname)
#
# v1.1          - Catch an invalid hostname in SRV record or .well-known/matrix/server
#               - Output of DNS SRV detection message optimized
#               - Check content type of .well-known/matrix/server found
#
# v1.0          - Only the version number has been changed.
#
# v0.8          - Support for IPv6 and command line arguments (-h and -v)
#
# v0.7          - Output of the redirect message optimized
#
# v0.6          - If the server is redirecting a request, the script will follow
#                 the redirect now
#               - Version number added to the output of the federation api tests
#               - Bugfix: Error messages of "jq" will be directed to /dev/null now
#
# v0.5          - Output of all message optimized (line lenght depends
#                 on available columns now)
#               - Check, if the hostname can be resolved, added
#
# v0.4          - Bugfix: Handle .well-known check bases on HTTP Code
#                         instead of return code (fixes HTTP 301)
#
# v0.3          - Function to determine the FQDN by using the API added
#               - Output of federation related tests optimized
#
# v0.2          - Tests for supported TLS versions added
#               - Output of status codes optimized
#
# v0.1          Initial release
#

set -o pipefail

DEBUG="" # use DEBUG="" to disable debugging or DEBUG="1" to activate


# Required tools
AWK_BIN=`which awk 2> /dev/null || echo "awk"`
BASENAME_BIN=`which basename 2> /dev/null || echo "basename"`
CURL_BIN=`which curl 2> /dev/null || echo "curl"`
DIG_BIN=`which dig 2> /dev/null || echo "dig"`
FOLD_BIN=`which fold 2> /dev/null || echo "fold"`
GREP_BIN=`which grep 2> /dev/null || echo "grep"`
JQ_BIN=`which jq 2> /dev/null || echo "jq"`
#NMAP_BIN=`which nmap 2> /dev/null || echo "nmap"`
OPENSSL_BIN=`which openssl 2> /dev/null || echo "openssl"`
PRINTF_BIN=`which printf 2> /dev/null || echo "printf"`
SED_BIN=`which sed 2> /dev/null || echo "sed"`
TPUT_BIN=`which tput 2> /dev/null || echo " tput"`
TIMEOUT_BIN=`which timeout 2> /dev/null || echo " timeout"`

EXECUTABLES_TO_CHECK="$AWK_BIN $BASENAME_BIN $CURL_BIN $DIG_BIN $FOLD_BIN $GREP_BIN $JQ_BIN $OPENSSL_BIN $PRINTF_BIN $SED_BIN $TIMEOUT_BIN $TPUT_BIN"

# Define some colors
BLUE="\\033[1;34m" # System messages
CYAN="\\033[1;36m" # Questions
GREEN="\\033[1;32m" # Success message
MAGENTA="\\033[1;35m" # Found devices or drivers (not used)
NORMAL="\\033[0;39m"
RED="\\033[1;31m" # Failure or error message
YELLOW="\\033[1;33m" # Warnings
WHITE="\\033[1;37m" # bold white (not used)

# Define position of status message
RES_COL=64
MOVE_TO_COL="echo -en \\033[${RES_COL}G"
SETCOLOR_SUCCESS="echo -en ${GREEN}"
SETCOLOR_FAILURE="echo -en ${RED}"
SETCOLOR_WARNING="echo -en ${YELLOW}"
SETCOLOR_NORMAL="echo -en ${NORMAL}"


# --- Do some basic checks ---
for program in $EXECUTABLES_TO_CHECK; do
  if [ ! -x "$program" ]; then
    echo "$0: The program \"$program\" is required, but missing. Exiting ..."
    exit 1
  fi
done

if [ "$EUID" -eq "0" ]; then
  echo "$0: This script must NOT be executed with root privileges. Exiting ..."
  exit 1
fi


# --- Initialize program ---

# Set script name
PROGRAM_NAME=`$BASENAME_BIN "$0"`
PROGRAM_VERSION="1.3"

FEDERATION_PORT_DEFAULT="8448"
TIMEOUT_DEFAULT="5" # Timeout for all network connections
IP_VERSION_DEFAULT="4" # default IP protocol if not specified

print_debug() {
# Print a message if the DEBUG flag is set. Else no-op

[ -n "$DEBUG" ] && echo "$@"
}

# Can we use colored output?
NUMBER_OF_COLORS=$($TPUT_BIN colors)
print_debug "# How many colors are supported?: $NUMBER_OF_COLORS"
if [ -n "$NUMBER_OF_COLORS" ] && [ $NUMBER_OF_COLORS -ge 8 ]; then
  COLORED_OUTPUT="true"
else
  COLORED_OUTPUT="false"
fi
print_debug "# Colored output mode has been set to \"$COLORED_OUTPUT\""

# --- Define functions ---
print_info() {
# Print either a help or a version screen. Use "help" as parameter
# to show the help screen or "version" for showing the version screen

  if [ "$#" -ne 1 ]; then
     print_debug "#   Wrong parameter(s)! Parameter(s) given: \"$@\""
    echo "$PROGRAM_NAME: Wrong parameter(s) in function \"$FUNCNAME\"!"
    exit 1
  fi

  case "$1" in
     help)
        echo "Usage: $0 [OPTION] [DOMAIN]"
        echo "Example: $0 -4 chat-secure.de"
        echo ""
        echo "Options:"
        echo -e "-4\t\tUse IPv4"
        echo -e "-6\t\tUse IPv6"
        echo -e "-h, --help\tShow this help"
        echo -e "-v, --version\tShow version number and exit"
        echo ""
        echo -e "Note:"
        echo -e " Using an IP literal as Domain is currently NOT supported."
        echo ""
        ;;
     version)
        echo "$PROGRAM_NAME v$PROGRAM_VERSION"
        echo "Copryright (c) 2018 Kai Kalua"
        echo "License: GNU GPL Version 2"
        exit
        ;;
     *)
       print_debug "#   Wrong parameter(s)! Parameter(s) given: \"$@\""
       echo "$PROGRAM_NAME: Wrong parameter(s) in function \"$FUNCNAME\"!"
       exit 1
       ;;
  esac

}

echo_success() {
  [ "$COLORED_OUTPUT" = "true" ] && $MOVE_TO_COL
  echo -n "[  "
  [ "$COLORED_OUTPUT" = "true" ] && $SETCOLOR_SUCCESS
  echo -n "OK"
  [ "$COLORED_OUTPUT" = "true" ] && $SETCOLOR_NORMAL
  echo -n "  ]"
  echo -ne "\n"
  return 0
}

echo_failure() {
  [ "$COLORED_OUTPUT" = "true" ] && $MOVE_TO_COL
  echo -n "["
  [ "$COLORED_OUTPUT" = "true" ] && $SETCOLOR_FAILURE
  echo -n "FAILED"
  [ "$COLORED_OUTPUT" = "true" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 1
}

echo_skipping() {
  [ "$COLORED_OUTPUT" = "true" ] && $MOVE_TO_COL
  echo -n "[ "
  [ "$COLORED_OUTPUT" = "true" ] && $SETCOLOR_WARNING
  echo -n "SKIP"
  [ "$COLORED_OUTPUT" = "true" ] && $SETCOLOR_NORMAL
  echo -n " ]"
  echo -ne "\n"
  return 0
}


print_message() {
# Print the given string and split the string before printing
# if necessary. This functions is only used in combination
# with the functions print_success(), print_warning() and
# print_failure().

  local WIDTH=`$TPUT_BIN cols`
  local MESSAGE_SPLITTED
  declare -a MESSAGE_ARRAY

  if [ "$?" -eq 0 ]; then
    WIDTH=$(($WIDTH-8))
  else
    WIDTH=79
  fi

  if [ "x$1" = "x" ]; then
    # Nothing to print
    print_debug "# No parameter passed to \"$FUNCNAME\""
  else
    MESSAGE_SPLITTED=`echo "$1" | $FOLD_BIN --spaces --width=$WIDTH -`
    IFS=$'\n'
    set -f
    MESSAGE_ARRAY=($MESSAGE_SPLITTED)
    unset IFS
    for message_line in "${MESSAGE_ARRAY[@]}"; do
      echo -n -e "\t $message_line\n"
    done

  fi
}


print_success() {
# Print the given string either colored or normal as
# success message

  if [ "x$1" = "x" ]; then
    # Nothing to print
    print_debug "# No parameter passed to \"$FUNCNAME\""
  else
    [ "$COLORED_OUTPUT" = "true" ] \
    && echo -e -n "${GREEN}[OK]${NORMAL}" \
    || echo -e -n "[OK]"

    # Print the rest of the message
    print_message "${1}"
  fi
}

print_failure() {
# Print the given string either colored or normal as
# failure message

  if [ "x$1" = "x" ]; then
    # Nothing to print
    print_debug "# No parameter passed to \"$FUNCNAME\""
  else
    [ "$COLORED_OUTPUT" = "true" ] \
    && echo -e -n "${RED}[FAIL]${NORMAL}" \
    || echo -e -n "[FAIL]"

    # Print the rest of the message
    print_message "${1}"
  fi

}

print_warning() {
# Print the given string either colored or normal as
# warning message

  if [ "x$1" = "x" ]; then
    # Nothing to print
    print_debug "# No parameter passed to \"$FUNCNAME\""
  else
    [ "$COLORED_OUTPUT" = "true" ] \
    && echo -e -n "${YELLOW}[WARN]${NORMAL}" \
    || echo -e -n "[WARN]"

    # Print the rest of the message
    print_message "${1}"
  fi

}

clean_exit_on_abort() {
# Once the test has been started and we can't continue the tests,
# we'll we use this function to leave the script.

  print_debug "# Betrete Funktion \"$FUNCNAME\""

  if [ "x$1" = "x" ]; then
    # Nothing to print
    print_debug "# No parameter passed to \"$FUNCNAME\""
    exit 1
  else
    [ "$COLORED_OUTPUT" = "true" ] \
     && echo -e -n "\n${BLUE}Tests aborted.${NORMAL}\n\n" \
     || echo -e -n "\nTests aborted.\n\n"

    exit $1
  fi
}


is_ipv4_address() {
# Check roughly, if the given parameter is a (valid) IPv4 address

  local REGEX='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
  local RC=2 # initialize to error in function

  if [ "x$1" = "x" ]; then
    # Nothing to print
    print_debug "# No parameter passed to \"$FUNCNAME\""
    exit 1
  else
    # If the variable or string on the right hand side of =~ operator
    # is quoted, it will be treated as a string instead of regex.
    # Hence don't quote.
    if [[ $1 =~ $REGEX ]]; then
      RC=0
    else
      RC=1
    fi
  fi # of if [ "x$1" = "x" ]; then

  print_debug "# is_ipv4_address: $RC"

  return $RC

} # of is_ipv6_address()


is_ipv6_address() {
# Check roughly, if the given parameter is a (valid) IPv6 address

  local REGEX='^([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4}$'
  local RC=2 # initialize to error in function

  if [ "x$1" = "x" ]; then
    # Nothing to print
    print_debug "# No parameter passed to \"$FUNCNAME\""
    exit 1
  else
    # If the variable or string on the right hand side of =~ operator
    # is quoted, it will be treated as a string instead of regex.
    # Hence don't quote.
    if [[ $1 =~ $REGEX ]]; then
      RC=0
    else
      RC=1
    fi
  fi # of if [ "x$1" = "x" ]; then

  print_debug "# is_ipv6_address: $RC"

  return $RC

} # of is_ipv6_address()


# --- MAIN ---

# Check amount of command line arguments (roughly)
#if [ -z "$1" ] || [ $# -gt 2 ]; then
#  print_info help
#  exit 1
#fi

# Parse command line arguments (we are lazy here and we
# will not use getopt[s]. Hence the user can not change
# the sequence of the arguments or choosen bad combinations).

# Credits to Bigwheel (https://stackoverflow.com/users/4006322/bigwheel)
# for providing the idea of the POSITIONAL array.
POSITIONAL=() # An array holding every parameter not beeing an option
while [[ $# -gt 0 ]]; do
  KEY="$1"
  case $KEY in
      -4)
        IP_VERSION_DEFAULT="4"
        shift # past argument
        ;;
      -6)
        IP_VERSION_DEFAULT="6"
        shift # past argument
        ;;
      -h|--help)
        print_info help
        exit 0
        ;;
      -v|--version)
        print_info version
        exit 0
        ;;
      *)    # not an option or an unknown option
        POSITIONAL+=("$1") # push it in our array for later usage
        shift # past argument
        ;;
  esac # of case $KEY in
done # of while [[ $# -gt 0 ]]; do
print_debug "# Using IPv${IP_VERSION_DEFAULT} for curl and openssl."

set -- "${POSITIONAL[@]}" # restore positional parameters

# Check amount of command line arguments again (only one should be left)
if [ -z "$1" ] || [ $# -ne 1 ]; then
  print_debug " # After parsing all command line arguments there are \"$#\" left. This is wrong."
  print_info help
  exit 1
fi

# ToDo: Remove a minus at the from front of $1 if there's one

# How many dots in the given domain name?
# (we will use $DOMAIN_DOT_COUNT later)
DOMAIN_DOT_COUNT="${1//[^.]}"
print_debug "# How many dots in the given domain name?: ${#DOMAIN_DOT_COUNT}"

# Copy parameter to $DOMAIN
DOMAIN=$1

# Check, if the given domain is an IP literal (which is
# currently not supported by this script)
if is_ipv4_address "$DOMAIN" || is_ipv6_address "$DOMAIN"; then
  print_debug " # You can NOT use an IP literal (\"$DOMAIN\") as a domain."
  print_info help
  exit 1
fi


# ----------------------------------------------------------------------------

echo "================================="
echo "= Matrix Federation Tester v${PROGRAM_VERSION} ="
echo "=       (c) 2019 Kai Kalua      ="
echo "================================="


# Inform about the start of the tests
[ "$COLORED_OUTPUT" = "true" ] \
 && echo -e -n "\n${BLUE}Running tests using IPv${IP_VERSION_DEFAULT} ...${NORMAL}\n\n" \
 || echo -e -n "\nRunning tests ...\n\n"

if [ ${#DOMAIN_DOT_COUNT} -gt 1 ]; then
  print_warning "Are you sure that you specified a domain name?"
fi

if [ ${#DOMAIN_DOT_COUNT} -lt 1 ]; then
  print_failure "\"${DOMAIN}\" is not a valid domain name. Exiting ..."
  clean_exit_on_abort 51
fi

# Check, if we can resolve the given domain name
RESULT_DIG_CHECK=`$DIG_BIN +noall +answer "${DOMAIN}"`
# If dig can resolve the domain or hostname it will return something, if not
# $RESULT_DIG_CHECK will be empty (in both cases the result of dig will be "0")
if [ -z "$RESULT_DIG_CHECK" -o "x$RESULT_DIG_CHECK" = "x" ]; then
    print_failure "There is no DNS entry for the given domain \"${DOMAIN}\". Exiting ..."
    clean_exit_on_abort 51
fi

# Check for .well-known configuration
# The following simple curl call will NOT deal with HTTP 301 and so on
#$CURL_BIN -${IP_VERSION_DEFAULT} -f --max-time $TIMEOUT_DEFAULT --insecure -XGET "https://${DOMAIN}/.well-known/matrix/server" > /dev/null 2>&1
HTTP_CODE=`$CURL_BIN -${IP_VERSION_DEFAULT} -f --max-time $TIMEOUT_DEFAULT --silent --output /dev/null --write-out "%{http_code}" -XGET "https://${DOMAIN}/.well-known/matrix/server"`
print_debug "# $CURL_BIN -${IP_VERSION_DEFAULT} -f --max-time $TIMEOUT_DEFAULT --silent --output /dev/null --write-out \"%{http_code}\" -XGET \"https://${DOMAIN}/.well-known/matrix/server\" returned HTTP-Code $HTTP_CODE."
if [ "$HTTP_CODE" -eq 301 ]; then
  print_warning "Server-side redirection for https://${DOMAIN}/.well-known/matrix/server. You should not do so. Following anyhow ..."
  # Repeat the check but follow the redirect this time
  HTTP_CODE=`$CURL_BIN -${IP_VERSION_DEFAULT} -L -f --max-time $TIMEOUT_DEFAULT --silent --output /dev/null --write-out "%{http_code}" -XGET "https://${DOMAIN}/.well-known/matrix/server"`
fi

if [ "$HTTP_CODE" -ne 200 ]; then
  print_warning "https://${DOMAIN}/.well-known/matrix/server not found."
  WELL_KNOWN_FOUND=""
else
  print_success "https://${DOMAIN}/.well-known/matrix/server found."
  WELL_KNOWN_FOUND="true"
fi

if [ -n "$WELL_KNOWN_FOUND" ]; then
  # Check Content Type (HTTP Response Header), but only except the first
  # Content-Type header line (sed '1b;/^Content-Type/d'), if there're more than one
  CONTENT_TYPE=`$CURL_BIN -I -${IP_VERSION_DEFAULT} -L --max-time $TIMEOUT_DEFAULT --insecure --silent -XGET "https://${DOMAIN}/.well-known/matrix/server" 2> /dev/null | $GREP_BIN -i "^Content-Type" | $SED_BIN '1b;/^Content-Type/d' | $AWK_BIN '{ print $2}'`

  print_debug "# Content type (raw): $CONTENT_TYPE"
  # If the content type contains additional information like charset remove it
  CONTENT_TYPE=${CONTENT_TYPE%;}
  print_debug "# Content type (stripped 1): $CONTENT_TYPE"
  # Remove carrige return
  CONTENT_TYPE=`echo $CONTENT_TYPE | $SED_BIN -e 's/\r$//g'`
  print_debug "# Content type (stripped 2): $CONTENT_TYPE"

  if [ -z "$CONTENT_TYPE" -o "x$CONTENT_TYPE" = "x" ]; then
    print_warning "No content-type entity header given for https://${DOMAIN}/.well-known/matrix/server"
  else
    if [ "x$CONTENT_TYPE" != "xapplication/json" ]; then
      print_success "Wrong content-type entity header ($CONTENT_TYPE) given for https://${DOMAIN}/.well-known/matrix/server"
    else
      print_success "Correct content-type entity header ($CONTENT_TYPE) given for https://${DOMAIN}/.well-known/matrix/server"
    fi
  fi # of if [ -z "$CONTENT_TYPE" -o "x$CONTENT_TYPE" = "x" ]; then

  # Check JSON syntax and content
  RC=`$CURL_BIN -${IP_VERSION_DEFAULT} -L --max-time $TIMEOUT_DEFAULT --insecure --silent -XGET "https://${DOMAIN}/.well-known/matrix/server" | $JQ_BIN -e '."m.server"' 2> /dev/null`
  if [ "$?" -ne 0 ]; then
    print_failure "Invalid JSON syntax or configuration of \"m.server\". Exiting ..."
    clean_exit_on_abort 52
  fi

  # Get .well-known configuration
  MATRIX_HOMESERVER_FQDN=`$CURL_BIN -${IP_VERSION_DEFAULT} -L --silent --max-time $TIMEOUT_DEFAULT --insecure -XGET "https://${DOMAIN}/.well-known/matrix/server" 2> /dev/null | $JQ_BIN -e '."m.server"' 2> /dev/null`
  # Remove all " from string
  MATRIX_HOMESERVER_FQDN=${MATRIX_HOMESERVER_FQDN//\"/}
  if [ -z "$MATRIX_HOMESERVER_FQDN" -o "x$MATRIX_HOMESERVER_FQDN" = "x" ]; then
    print_failure "Something went wrong during reading your JSON configuration."
    clean_exit_on_abort 53
  else
    print_success "Your homeserver FQDN for federation is $MATRIX_HOMESERVER_FQDN"
  fi

else
  # Try to get SRV record (substr is used to remove the last dot from the FQDN)
  MATRIX_HOMESERVER_FQDN=`$DIG_BIN +short SRV _matrix._tcp.${DOMAIN} | $AWK_BIN ' { print substr($4, 1, length($4)-1) ":" $3 }'`
  if [ -z "$MATRIX_HOMESERVER_FQDN" -o "x$MATRIX_HOMESERVER_FQDN" = "x" ]; then
    print_warning "DNS SRV record _matrix._tcp.${DOMAIN} is invalid or does not exist"
    print_success "Continuing with ${DOMAIN}:${FEDERATION_PORT_DEFAULT} as homeserver FQDN for federation"
    # Set fallback to the given domain, port $FEDERATION_PORT_DEFAULT (there were no .well-known
    # configuration as well as no DNS SRV entry for matrix, which is REALLY bad
    MATRIX_HOMESERVER_FQDN=${DOMAIN}:${FEDERATION_PORT_DEFAULT}
    DNS_SRV_FOUND=""
  else
    print_success "DNS SRV record _matrix._tcp.${DOMAIN} found."
    print_success "Your homeserver FQDN for federation is $MATRIX_HOMESERVER_FQDN"
    DNS_SRV_FOUND="true"
  fi
fi

# Check, if $MATRIX_HOMESERVER_FQDN contains a port, too. If not, add the default port
if ! [[ $MATRIX_HOMESERVER_FQDN =~ ":" ]]; then
  print_warning "Port missing for \"$MATRIX_HOMESERVER_FQDN\". Adding default port ($FEDERATION_PORT_DEFAULT)."
  MATRIX_HOMESERVER_FQDN=${MATRIX_HOMESERVER_FQDN}:${FEDERATION_PORT_DEFAULT}
fi

# Check, if the port is open

#   Separate FQDN and Port
MATRIX_HOMESERVER_FQDN_WITHOUT_PORT=${MATRIX_HOMESERVER_FQDN%\:*}
MATRIX_HOMESERVER_PORT=${MATRIX_HOMESERVER_FQDN#*\:}

# Check, if if the hostname of the homeserver is a least a domain name
# (catch a wrong configuration of SRV or .well-known/matrix/server)
DOMAIN_DOT_COUNT="${MATRIX_HOMESERVER_FQDN_WITHOUT_PORT//[^.]}"
if [ ! ${#DOMAIN_DOT_COUNT} -ge 1 ]; then
  print_failure "Either your SRV DNS record or your .well-known-configuration uses an invalid hostname ($MATRIX_HOMESERVER_FQDN_WITHOUT_PORT). Exiting ..."
  clean_exit_on_abort 54
fi

#   Check, if the port is open
if [ -z "$MATRIX_HOMESERVER_FQDN_WITHOUT_PORT" -o "x$MATRIX_HOMESERVER_FQDN_WITHOUT_PORT" = "x" -o -z "$MATRIX_HOMESERVER_PORT" -o "x$MATRIX_HOMESERVER_PORT" = "x" ]; then
  print_warning "Unable to separate FQDN and port of your homeserver. Can't check for open port."
else
  $TIMEOUT_BIN $TIMEOUT_DEFAULT bash -c "cat < /dev/null > /dev/tcp/${MATRIX_HOMESERVER_FQDN_WITHOUT_PORT}/${MATRIX_HOMESERVER_PORT} > /dev/null 2>&1"
  if [ "$?" -ne 0 ]; then
    print_failure "Port \"${MATRIX_HOMESERVER_PORT}\" is closed on your homeserver. Exiting ..."
    clean_exit_on_abort 54
  else
    print_success "Port \"${MATRIX_HOMESERVER_PORT}\" of your homeserver is open as it should be."
  fi
fi  # of if [ -z "$MATRIX_HOMESERVER_FQDN_WITHOUT_PORT" -o ...


# Check SSL certificate
RESULT_OPENSSL_CHECK=`$TIMEOUT_BIN $TIMEOUT_DEFAULT $OPENSSL_BIN  s_client -${IP_VERSION_DEFAULT} -connect $MATRIX_HOMESERVER_FQDN < /dev/null 2> /dev/null | $GREP_BIN "Verify return code:" | $AWK_BIN  '{print $4}'`
if [ "$?" -ne 0 -o -z "$RESULT_OPENSSL_CHECK" ]; then
  print_failure "Could not reach \"$MATRIX_HOMESERVER_FQDN\" using IPv${IP_VERSION_DEFAULT}"
  clean_exit_on_abort 55
fi

# Convert the content of $RESULT_SSL_CHECK to an integer (it should already be
# an integer, but we try to be a little bit more fail safe)
RC_OPENSSL_CHECK=`$PRINTF_BIN '%d\n' "$RESULT_OPENSSL_CHECK" 2>/dev/null`
if [ "$RC_OPENSSL_CHECK" -ne 0 ]; then
    print_failure "SSL certificate of is $MATRIX_HOMESERVER_FQDN is invalid. Exiting ..."
    clean_exit_on_abort 56
else
    print_success "SSL certificate of $MATRIX_HOMESERVER_FQDN successfully validated"
fi

# Add an additional check of the certificate because of a strange behavior
# of synapse: If you're using a DNS SRV record only (without .well-known-configuration(
# the certificate MUST be issued to the domain name according to the Federation API
if [ -n "$DNS_SRV_FOUND" ]; then

  RESULT_OPENSSL_CHECK=`$TIMEOUT_BIN $TIMEOUT_DEFAULT $OPENSSL_BIN  s_client -${IP_VERSION_DEFAULT} -verify_hostname ${DOMAIN} -connect $MATRIX_HOMESERVER_FQDN < /dev/null 2> /dev/null | $GREP_BIN "Verify return code:" | $AWK_BIN  '{print $4}'`
  if [ "$?" -ne 0 -o -z "$RESULT_OPENSSL_CHECK" ]; then
    print_failure "Could not reach \"$MATRIX_HOMESERVER_FQDN\" using IPv${IP_VERSION_DEFAULT}"
    clean_exit_on_abort 58
  fi

  # Convert the content of $RESULT_SSL_CHECK to an integer (it should already be
  # an integer, but we try to be a little bit more fail safe)
  RC_OPENSSL_CHECK=`$PRINTF_BIN '%d\n' "$RESULT_OPENSSL_CHECK" 2>/dev/null`
  if [ "$RC_OPENSSL_CHECK" -ne 0 ]; then
      print_failure "SSL certificate of $MATRIX_HOMESERVER_FQDN is not valid for \"${DOMAIN}\". This is necessary if you are using DNS SRV record. Otherwise synapse will not federate."
  else
      print_success "SSL certificate of $MATRIX_HOMESERVER_FQDN is valid for \"${DOMAIN}\"."
  fi

fi # of if [ -n "$DNS_SRV_FOUND" ]; then


# Check supported TLS versions
declare -A TLS_VERSIONS # -A = define as an associative array
TLS_VERSIONS_TO_CHECK="ssl3 tls1 tls1_1 tls1_2 tls1_3"

for tls_version in $TLS_VERSIONS_TO_CHECK; do
  RESULT_TLS_VERSION_CHECK=`$TIMEOUT_BIN $TIMEOUT_DEFAULT $OPENSSL_BIN s_client -${IP_VERSION_DEFAULT} -connect $MATRIX_HOMESERVER_FQDN -$tls_version < /dev/null 2> /dev/null`
  RC_TLS_VERSION_CHECK=$?
  TLS_VERSIONS["$tls_version"]=$RC_TLS_VERSION_CHECK
  TLS_VERSIONS["$tls_version result"]=$RESULT_TLS_VERSION_CHECK
done

for tls_version in $TLS_VERSIONS_TO_CHECK; do
   case $tls_version in
    ssl3)
        if [ ${TLS_VERSIONS["$tls_version"]} -eq 0 ]; then
            # We don't care about the cipher. SSLv3 is BAD.
            print_failure "SSLv3 supported, but SSLv3 is EXTREMLY UNSECURE. You SHOULD disable it."
        else
            print_success "SSLv3 not supported. Good (it's extremly unsecure)."
        fi
        ;;
    tls1)
        if [ ${TLS_VERSIONS["$tls_version"]} -eq 0 ]; then
            TLS_CIPHER_USED=`echo ${TLS_VERSIONS["$tls_version result"]} |  awk '{for(i=1; i<=NF; i++) if($i~/Cipher/) { print $(i+2); exit}}'`
            print_failure "TLSv1.0 supported (Cipher: $TLS_CIPHER_USED), but TLSv1.0 is unsecure."
        else
            print_success "TLSv1.0 not supported. Good (it's unsecure)."
        fi
        ;;
    tls1_1)
        if [ ${TLS_VERSIONS["$tls_version"]} -eq 0 ]; then
            TLS_CIPHER_USED=`echo ${TLS_VERSIONS["$tls_version result"]} |  awk '{for(i=1; i<=NF; i++) if($i~/Cipher/) { print $(i+2); exit}}'`
            print_warning "TLSv1.1 supported (Cipher: $TLS_CIPHER_USED). Please consider to use TLS v1.2/1.3 only."
        else
            print_success "TLSv1.1 not supported. Very Good (it's less secure)."
        fi
        ;;
    tls1_2)
        if [ ${TLS_VERSIONS["$tls_version"]} -eq 0 ]; then
            TLS_CIPHER_USED=`echo ${TLS_VERSIONS["$tls_version result"]} |  awk '{for(i=1; i<=NF; i++) if($i~/Cipher/) { print $(i+2); exit}}'`
            print_success "TLSv1.2 supported (Cipher: $TLS_CIPHER_USED)."
        else
            print_warning "TLSv1.2 not supported. Currently this is a bad idea."
        fi
        ;;
    tls1_3)
        if [ ${TLS_VERSIONS["$tls_version"]} -eq 0 ]; then
            TLS_CIPHER_USED=`echo ${TLS_VERSIONS["$tls_version result"]} |  awk '{for(i=1; i<=NF; i++) if($i~/Cipher/) { print $(i+2); exit}}'`
            print_success "TLSv1.3 (Cipher: $TLS_CIPHER_USED) has been enabled. Perfect!"
        else
            print_success "TLSv1.3 not supported. Currently this is acceptable."
        fi
        ;;
    *)
        print_warning "Unhandled TLS version \"$tls_version\". This is a bug."
        ;;
   esac
done

# Get server name and version (we can use curl without "--insecure" now because of our
# sucessfull validation of the ssl certificate before), -L follows any redirect
RC=`$CURL_BIN -${IP_VERSION_DEFAULT} -L --max-time $TIMEOUT_DEFAULT --silent -XGET "https://${MATRIX_HOMESERVER_FQDN}/_matrix/federation/v1/version" | $JQ_BIN -e '.server .version' 2> /dev/null`
if [ "$?" -ne 0 ]; then
  print_failure "Invalid JSON syntax or configuration of \"_matrix/federation/v1/version\""
else
  MATRIX_HOMESERVER_SOFTWARE=`$CURL_BIN -${IP_VERSION_DEFAULT} -L -f --max-time $TIMEOUT_DEFAULT -XGET "https://${MATRIX_HOMESERVER_FQDN}/_matrix/federation/v1/version" 2> /dev/null | $JQ_BIN -e '.server .name' 2> /dev/null`
  # Remove all " from string
  MATRIX_HOMESERVER_SOFTWARE=${MATRIX_HOMESERVER_SOFTWARE//\"/}
  if [ -z "$MATRIX_HOMESERVER_SOFTWARE" -o "x$MATRIX_HOMESERVER_SOFTWARE" = "x" ]; then
    # We give only a warning because v1 is deprecated
    print_warning "[Federation-API v1] Unable to detect the name of your homeserver software."
  else
    print_success "[Federation-API v1] Your homeserver is running $MATRIX_HOMESERVER_SOFTWARE"
  fi

  MATRIX_HOMESERVER_VERSION=`$CURL_BIN -${IP_VERSION_DEFAULT} -L -f --max-time $TIMEOUT_DEFAULT -XGET "https://${MATRIX_HOMESERVER_FQDN}/_matrix/federation/v1/version" 2> /dev/null | $JQ_BIN -e '.server .version' 2> /dev/null`
  # Remove all " from string
  MATRIX_HOMESERVER_VERSION=${MATRIX_HOMESERVER_VERSION//\"/}
  if [ -z "$MATRIX_HOMESERVER_VERSION" -o "x$MATRIX_HOMESERVER_VERSION" = "x" ]; then
    # We give only a warning because v1 is deprecated
    print_warning "[Federation-API v1] Unable to detect the version of your homeserver software."
  else
    print_success "[Federation-API v1] Your homeserver is running version $MATRIX_HOMESERVER_VERSION."
  fi
fi # of if [ "$?" -ne 0 ]; then


# Get the FQDN of the homeserver by using the federation api
RC=`$CURL_BIN -${IP_VERSION_DEFAULT} -L --max-time $TIMEOUT_DEFAULT --silent -XGET "https://${MATRIX_HOMESERVER_FQDN}/_matrix/key/v2/server" | $JQ_BIN -e '.server_name' 2> /dev/null`
if [ "$?" -ne 0 ]; then
  print_failure "Invalid JSON syntax or configuration of \"_matrix/key/v2/server\""
else
  MATRIX_HOMESERVER_FQDN_FED_API=`$CURL_BIN -${IP_VERSION_DEFAULT} -L --max-time $TIMEOUT_DEFAULT --silent -XGET "https://${MATRIX_HOMESERVER_FQDN}/_matrix/key/v2/server" | $JQ_BIN -e '.server_name' 2> /dev/null`
  # Remove all " from string
  MATRIX_HOMESERVER_FQDN_FED_API=${MATRIX_HOMESERVER_FQDN_FED_API//\"/}
  if [ -z "$MATRIX_HOMESERVER_FQDN_FED_API" -o "x$MATRIX_HOMESERVER_FQDN_FED_API" = "x" ]; then
    print_failure "[Federation-API v2] Unable to detect the FQDN of your homeserver."
  else
    print_success "[Federation-API v2] Your homeserver FQDN is $MATRIX_HOMESERVER_FQDN_FED_API"
  fi
fi # of if [ "$?" -ne 0 ]; then

# Finished
[ "$COLORED_OUTPUT" = "true" ] \
 && echo -e -n "\n${BLUE}Tests finished.${NORMAL}\n\n" \
 || echo -e -n "\nTests finished.\n\n"
